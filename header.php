<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns#">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- noindexの設定 -->
    <?php if(is_tag() || is_date() || is_search() || is_404()) : ?>
        <meta name="robots" content="noindex"/>
    <?php endif; ?>
    <!-- SNS用metaデータ 共通OGPタグ -->
    <meta property="og:site_name" content="<?php bloginfo( 'name' ); ?>">
    <meta property="og:locale" content="ja_JP">
    <!-- Twitterカード　OGPタグ -->
    <meta name="twitter:site" content="@YuppyHappyToyou">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:creator" content="@YuppyHappyToyou">
    <!-- 個別ページ用のmetaデータ -->
    <?php if( is_single() || is_page() ): ?>
    <?php setup_postdata($post) ?>
        <meta name="description" content="<?php echo strip_tags( get_the_excerpt() ); ?>"> 
        <?php if( has_tag() ): ?>
        <?php $tags = get_the_tags();
        $kwds = array();
        foreach($tags as $tag){
            $kwds[] = $tag->name;
        } ?>
        <meta name="keywords" content="<?php echo implode( ',',$kwds ); ?>">
        <?php endif; ?>
        <!-- Twitterカード OGP -->
        <meta name="twitter:description" content="<?php echo strip_tags( get_the_excerpt() ); ?>">
        <?php if( has_post_thumbnail() ): ?>
        <?php $postthumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' ); ?>
        <meta name="twitter:image:src" content="<?php echo $postthumb[0]; ?>">
        <?php endif; ?>
        <!-- 投稿・固定ページ用OGPタグ -->
        <meta property="og:type" content="article">
        <meta property="og:title" content="<?php the_title(); ?>">
        <meta property="og:url" content="<?php the_permalink(); ?>">
        <?php if( has_post_thumbnail() ): ?>
            <?php $postthumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' ); ?>
            <meta property="og:image" content="<?php echo $postthumb[0]; ?>">
        <?php endif; ?>
    <!-- 個別ページ以外のmetaデータ(TOPページ・記事一覧ページなど) -->
    <?php else: ?>
        <meta name="description" content="<?php bloginfo( 'description' ); ?>">
        <?php $allcats = get_categories();
        $kwds = array();
        foreach( $allcats as $allcat ) {
            $kwds[] = $allcat->name;
        } ?>
        <meta name="twitter:description" content=<?php bloginfo( 'description' ) ?>">
        <meta name="twitter:image:src" content="<?php echo get_template_directory_uri(); ?>/add/title.png">
        <meta name="keywords" content="<?php echo implode( ',',$kwds ); ?>">
        <meta property="og:type" content="website">  
        <meta property="og:title" content="<?php bloginfo( 'name' ); ?>">
        <?php
        $http = is_ssl() ? 'https' . '://' : 'http' . '://';
        $url = $http . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URL"];
        ?>
        <meta property="og:url" content="<?php echo $url; ?>">
        <meta property="og:description" content="<?php bloginfo( 'description' ) ?>">
        <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/images/sample-image.png">
    <?php endif; ?>
    <!-- アイコン -->
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/webclipicon.png" />
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>
    <div class="header inner">
        <div class="site-title">
            <h1><a href="<?php echo home_url(); ?>">
                <?php bloginfo('name'); ?>”
            </a></h1>
        </div>
        <!-- スマートフォン用メニューボタン -->
        <button type="button" id="navbutton">
            <i class="fas fa-list-ul"></i>
        </button>
    </div>
    <!-- ヘッダーメニュー -->
    <?php wp_nav_menu( array(
        'theme_location' => 'header-nav',
        'container' => 'nav',
        'container_class' => 'header-nav',
        'container_id' => 'header-nav',
        'fallback_cd' => ''
    )); ?>
</header>